# vim: set ai et sw=3 ts=3 nu:
package RPM_MD;

use strict;
use warnings;
use IO::Uncompress::Gunzip;
use LWP::UserAgent;
use HTTP::Date;
use HTTP::Status;
use POSIX;
use XML::LibXML;
use Term::ProgressBar;

BEGIN {
   use Exporter();
   our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

   $VERSION     = 1.00;
   @ISA         = qw(Exporter);
   @EXPORT      = qw(&parse_rpmmd);
   %EXPORT_TAGS = ();
   @EXPORT_OK   = qw(&parse_rpmmd);

   our $parser = XML::LibXML->new();
   $parser->validation(0);
   $parser->no_network(1);
   $parser->load_ext_dtd(0);
}
our @EXPORT_OK;
our $parser;

use constant {
   NS_REPO   => 'http://linux.duke.edu/metadata/repo',
   NS_RPM    => 'http://linux.duke.edu/metadata/rpm',
   NS_COMMON => 'http://linux.duke.edu/metadata/common',
};

sub ct($$$) {
   my $node = shift;
   my $ns = shift;
   my $tag = shift;
   my @children = $node->getChildrenByTagNameNS($ns, $tag);
   if (@children and (scalar(@children) > 0)) {
      return $children[0]->textContent;
   } else {
      die "no $tag under ".($node->nodeName);
   }
}

sub cto($$$) {
   my $node = shift;
   my $ns = shift;
   my $tag = shift;
   my @children = $node->getChildrenByTagNameNS($ns, $tag);
   if (@children and (scalar(@children) > 0)) {
      return $children[0]->textContent;
   } else {
      return undef;
   }
}

sub ce($$$) {
   my $node = shift;
   my $ns = shift;
   my $tag = shift;
   my @children = $node->getChildrenByTagNameNS($ns, $tag);
   if (@children and (scalar(@children) > 0)) {
      return $children[0];
   } else {
      die "no $tag under ".($node->nodeName);
   }
}

sub ceo($$$) {
   my $node = shift;
   my $ns = shift;
   my $tag = shift;
   my @children = $node->getChildrenByTagNameNS($ns, $tag);
   if (@children and (scalar(@children) > 0)) {
      return $children[0];
   } else {
      return undef;
   }
}

sub _a($$$) {
   my $h = shift;
   my $k = shift;
   my $v = shift;
   $h->{$k} = $v if defined $v;
}

sub parse_rpmmd($$$$) {
   my $r = shift;
   my $h = shift;
   my $ua = shift;
   my $verbose = shift;

   die "no baseurl ?" unless exists $r->{baseurl} and defined $r->{baseurl};
   my $repomd_xml_url = $r->{baseurl}.'/repodata/repomd.xml';

   print "\t", "downloading ", $repomd_xml_url, ": " if $verbose;
   my %headers = ();
   if (not $r->{flags}->{deep}) {
      # don't even set the etag headers as it would prevent the server from giving us the content
      $headers{'If-Modified-Since'} = $h->{last_modified} if exists $h->{last_modified} and defined $h->{last_modified};
      $headers{'Etag'} = $h->{etag} if exists $h->{etag} and defined $h->{etag};
   }

   my $resp = $ua->get($repomd_xml_url, %headers) or die "failed to get $repomd_xml_url: $!";

   my $ri = {};

   my $repomd_timestamp = undef;
   my $repomd_lastmod = undef;
   my $repomd_location = undef;
   my $repomd_checksum = undef;
   my $repomd_checksum_type = undef;

   my $needs_processing = undef;
   print $resp->status_line, "\n" if $verbose;
   if ((not $r->{flags}->{deep}) and ($resp->code eq RC_NOT_MODIFIED() or ($resp->is_success and exists $h->{last_modified} and defined $h->{last_modified} and $h->{last_modified} ne $resp->last_modified))) {
      print "\t", "not modified", "\n" if $verbose;
      return undef;
   } elsif ($resp->code eq RC_NOT_MODIFIED() or $resp->is_success) {
      my $content = $resp->content;
      {
         #my $tree = $parser->load_xml(string => $content);
         my $tree = $parser->parse_string($content);
         my $root = $tree->getDocumentElement();
         foreach my $data ($root->getChildrenByTagNameNS(NS_REPO, 'data')) {
            next unless $data->getAttribute('type') eq 'primary';
            my $location = ce($data, NS_REPO, 'location');
            $repomd_location = $location->getAttribute('href');
            $repomd_timestamp = ct($data, NS_REPO, 'timestamp');
            my $checksum = ce($data, NS_REPO, 'checksum');
            $repomd_checksum = $checksum->textContent;
            $repomd_checksum_type = $checksum->getAttribute('type');
         }
         my $revision = cto($root, NS_REPO, 'revision');
         $ri->{timestamp} = defined $revision ? $revision : $repomd_timestamp;
         $ri->{last_modified} = $resp->header('Last-Modified');
         $ri->{etag} = $resp->header('Etag');
      }

      if (exists $h->{timestamp} and defined $h->{timestamp} and $h->{timestamp} eq $repomd_timestamp) {
         print "\t", "not modified: timestamp=".$repomd_timestamp, "\n";
         return undef;
      }
   } else {
      warn "failed to retrieve $repomd_xml_url: ".$resp->status_line;
      return {};
   }
  
   my $primary_url = $r->{baseurl}.'/'.$repomd_location;

   $ua->show_progress(1) if $verbose;
   my $primary_resp = $ua->get($primary_url);
   $ua->show_progress(0);

   if (not $primary_resp->is_success) {
      warn "failed to download $primary_url: ".$primary_resp->status_line;
      return {};
   }

   my $content_ref = $primary_resp->content_ref;
   my $z = new IO::Uncompress::Gunzip($content_ref);

   my $tree = $parser->parse_fh($z);
   my $root = $tree->getDocumentElement();
   my $total = $root->getAttribute("packages");

   my $progress = undef;
   if ($verbose) {
      $progress = Term::ProgressBar->new({
         count => $total,
         name  => "        parsing XML",
         ETA   => 'linear',
      });
      $progress->minor(0);
   }

   my @packages = ();
   foreach my $package ($root->getChildrenByTagNameNS(NS_COMMON, 'package')) {
      $progress->update() if defined $progress;

      my $name = ct($package, NS_COMMON, 'name');
      my $arch = ct($package, NS_COMMON, 'arch');

      my $d = ();
      $d->{name} = $name;
      $d->{arch} = $arch;

      my $version_elt = ce($package, NS_COMMON, 'version');
      $d->{version} = $version_elt->getAttribute('ver');
      $d->{release} = $version_elt->getAttribute('rel');
      $d->{summary} = HTML::Entities::encode_numeric(ct($package, NS_COMMON, 'summary'));
      my $packager = ct($package, NS_COMMON, 'packager');
      $d->{packager} = $packager if $packager and length($packager) > 0;

      $d->{description} = HTML::Entities::encode_numeric(ct($package, NS_COMMON, 'description'));

      my $format = ce($package, NS_COMMON, 'format');
      $d->{vendor} = HTML::Entities::encode_numeric(ct($format, NS_RPM, 'vendor'));

      my $size_elt = ce($package, NS_COMMON, 'size');
      $d->{size_package} = $size_elt->getAttribute('package');
      $d->{size_installed} = $size_elt->getAttribute('installed');

      my $location_elt = ce($package, NS_COMMON, 'location');
      $d->{location} = $r->{baseurl}.'/'.$location_elt->getAttribute('href');

      $d->{buildtime} = strftime("%Y-%m-%dT%H:%M:%S.000Z", gmtime(ce($package, NS_COMMON, 'time')->getAttribute('build')));

      my $url_elt = ce($package, NS_COMMON, 'url');
      if ($url_elt) {
         my $url = $url_elt->textContent;
         HTML::Entities::encode_numeric($url);
         $d->{url} = $url if $url and length($url) > 0;
      }

      my $sha = ct($package, NS_COMMON, 'checksum');
      $d->{sha} = $sha;

      _a($d, 'sourcerpm', cto($format, NS_RPM, 'sourcerpm'));
      _a($d, 'rpmgroup', cto($format, NS_RPM, 'group'));
      _a($d, 'license', cto($format, NS_RPM, 'license'));

      #while (my ($k, $v) = each(%$d)) { die "undefined value for key $k" unless defined $v; }

      $d->{file} = [];
      foreach my $file (map { $_->textContent } $format->getChildrenByTagNameNS(NS_COMMON, 'file')) {
         push(@{$d->{file}}, $file);
      }

      for my $tag (qw(provides requires supplements conflicts obsoletes recommends)) {
         $d->{$tag} = [];
         my $e = ceo($format, NS_RPM, $tag);
         next unless defined $e;
         foreach my $entry ($e->getChildrenByTagNameNS(NS_RPM, 'entry')) {
            push(@{$d->{$tag}}, $entry->getAttribute('name'));
         }
      }

      $d->{id} = $r->{repoid} . '-' . $sha;

      push(@packages, $d);
   }
   close($z);

   return [\@packages, $ri];
}

1;
