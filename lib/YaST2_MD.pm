# vim: set ai et sw=3 ts=3 nu:
package YaST2_MD;

use strict;
use warnings;
use IO::Uncompress::Gunzip;
use LWP::UserAgent;
use HTTP::Status;
use POSIX;
use Term::ProgressBar;

BEGIN {
    use Exporter();
    our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

    $VERSION     = 1.00;
    @ISA         = qw(Exporter);
    @EXPORT      = qw(&parse_y2md);
    %EXPORT_TAGS = ();
    @EXPORT_OK   = qw(&parse_y2md);
}
our @EXPORT_OK;

sub parse_y2md($$$$) {
   my $r = shift;
   my $h = shift;
   my $ua = shift;
   my $verbose = shift;

   my $ri = {};

   my $descrdir = undef;
   {
      my $url = $r->{baseurl}.'/content';
      my %headers = ();
      if (not $r->{flags}->{deep}) {
         $headers{'If-Modified-Since'} = $h->{last_modified} if exists $h->{last_modified} and defined $h->{last_modified};
         $headers{'Etag'} = $h->{etag} if exists $h->{etag} and defined $h->{etag};
      }
      
      print "\t", "downloading ", $url, ": " if $verbose;
      my $resp = $ua->get($url, %headers) or die "failed to get $url: $!";
      print $resp->status_line, "\n" if $verbose;
      
      if ((not $r->{flags}->{deep}) and ($resp->code eq RC_NOT_MODIFIED() or ($resp->is_success and exists $h->{last_modified} and defined $h->{last_modified} and $h->{last_modified} >= $resp->last_modified))) {
         print "\t", "not modified", "\n" if $verbose;
         return undef;
      } elsif ($resp->code eq RC_NOT_MODIFIED() or $resp->is_success) {
         $ri->{last_modified} = $resp->header('Last-Modified');
         $ri->{timestamp} = $ri->{last_modified};
         $ri->{etag} = $resp->header('Etag');
      } else {
         warn "failed to get $url: ".$resp->status_line;
         return {};
      }

      foreach (split(/[\n\r]+/, ${$resp->content_ref})) {
         if (/^DESCRDIR\s+(.+)\s*/) {
            $descrdir = $1;
            last;
         }
      }
      die "missing DESCRDIR in $url" unless defined $descrdir;
   }
   my %packages = ();
   {
      my $url = $r->{baseurl}.'/'.$descrdir.'/packages.gz';
      $ua->show_progress(1) if $verbose;
      my $resp = $ua->get($url) or die "failed to get $url: $!";
      $ua->show_progress(0);

      if (not $resp->is_success) {
         warn "failed to get $url: ".$resp->status_line;
         return {};
      }

      open(my $sh, '<', $resp->content_ref) or die "failed to open scalar: $!";
      my $z = new IO::Uncompress::Gunzip($sh); #$resp->content_ref);
      my $c = undef;
      my $progress = undef;
      my $length = $resp->content_length;
      if ($verbose) {
         $progress = Term::ProgressBar->new({
            count => $length,
            name  => "   parsing packages",
            ETA   => 'linear',
         });
         $progress->minor(0);
      }

      my $i = 0;
      while (<$z>) {
         $i++;
         $progress->update($sh->tell) if $progress and $i % 10 == 0;
         next if /^#/;
         next if /^=(Ver):/;

         next if /^\+Ins:/ .. /^-Ins:/;
         next if /^\+Del:/ .. /^-Del:/;
         next if /^\+Kwd:/ .. /^-Kwd:/;
         next if /^\+Aut:/ .. /^-Aut:/;
         next if /^\+Eul:/ .. /^-Eul:/;

         chomp;
         my $gtag = undef;
         $gtag = 1 if /^[\+\-]\S+:/;
         s/^\s+$// if /^=/;

         if (/^=Pkg:\s*(.+?)\s+(.+?)\s+(.+?)\s+(.+?)\s*$/) {
            $packages{$c->{id}} = $c if defined $c;
            $c = {
               id          => join('-', ($1, $2, $3, $4)),
               name        => $1,
               version     => $2,
               release     => $3,
               arch        => $4,
               requires    => [],
               provides    => [],
               file        => [],
               obsoletes   => [],
               supplements => [],
               conflicts   => [],
               recommends  => [],
               suggests    => [],
               enhances    => [],
            };
         } elsif (/^=Cks:\s+\S+\s+(\S+)\s*$/) {
            $c->{sha} = $1;
         } elsif (/^=Loc:\s*(\d+)\s+(.+)\s*$/) {
            $c->{location} = $r->{baseurl}.'/'.$1;
         } elsif (/^=Src:\s*(.+?)\s+(.+?)\s+(.+?)\s+(.+?)\s*/) {
            $c->{sourcerpm} = {
               name    => $1,
               version => $2,
               release => $3,
               arch    => $4,
            };
         } elsif (/^=Siz:\s*(\d+)\s+(\d+)\s*$/) {
            $c->{size_package} = $1;
            $c->{size_installed} = $2;
         } elsif (/^=Shr:\s*(.+?)\s+(.+?)\s+(.+?)\s+(.+?)\s*$/) {
            $c->{REF} = {
               name    => $1,
               version => $2,
               release => $3,
               arch    => $4,
            };
         } elsif (/^=Tim:\s*(\d+)\s*$/) {
            $c->{buildtime} = strftime("%Y-%m-%dT%H:%M:%S.000Z", gmtime($1));
         } elsif (/^=Vnd:\s*(.+)\s*$/) {
            $c->{vendor} = $1;
         } elsif (/^=Lic:\s*(.+)\s*$/) {
            $c->{license} = $1;
         } elsif (/^=Grp:\s*(.+)\s*$/) {
            $c->{rpmgroup} = $1;
         } elsif (/^\+Req:\s*$/ .. /^-Req:\s*$/ or /^\+Prq:\s*$/ .. /^-Prq:\s*$/ and not $gtag) {
            push(@{$c->{requires}}, $_);
         } elsif (/^\+Prv:\s*$/ .. /^-Prv:\s*$/ and not $gtag) {
            my $l = $_;
            push(@{$c->{provides}}, $l);
            push(@{$c->{file}}, $l) if /^(\/.+)\s*$/;
         } elsif (/^\+Obs:\s*$/ .. /^-Obs:\s*$/ and not $gtag) {
            push(@{$c->{obsoletes}}, $_);
         } elsif (/^\+Sup:\s*$/ .. /^-Sup:\s*$/ and not $gtag) {
            push(@{$c->{supplements}}, $_);
         } elsif (/^\+Con:\s*$/ .. /^-Con:\s*$/ and not $gtag) {
            push(@{$c->{conflicts}}, $_);
         } elsif (/^\+Rec:\s*$/ .. /^-Rec:\s*$/ and not $gtag) {
            push(@{$c->{recommends}}, $_);
         } elsif (/^\+Sug:\s*$/ .. /^-Sug:\s*$/ and not $gtag) {
            push(@{$c->{suggests}}, $_);
         } elsif (/^\+Enh:\s*$/ .. /^-Enh:\s*$/ and not $gtag) {
            push(@{$c->{enhances}}, $_);
         }
      }
      close($z);
      close($sh);
      $progress->update($length) if $progress;
      $packages{$c->{id}} = $c if defined $c;
   }

   {
      my $url = $r->{baseurl}.'/'.$descrdir.'/packages.en.gz';
      my $resp = $ua->get($url) or die "failed to get $url: $!";
      if (not $resp->is_success) {
         die "failed to retrieve $url: ".$resp->status_line;
         return {};
      }

      open(my $sh, '<', $resp->content_ref) or die "failed to open scalar: $!";
      my $z = new IO::Uncompress::Gunzip($sh);
      my $c = undef;
      my $progress = undef;
      my $length = $resp->content_length;
      if ($verbose) {
         $progress = Term::ProgressBar->new({
            count => $length,
            name  => "parsing packages.en",
            ETA   => 'linear',
         });
         $progress->minor(0);
      }

      my $i = 0;
      while (<$z>) {
         $i++;
         $progress->update($sh->tell) if $progress and $i % 10 == 0;
         next if /^#/;
         next if /^=Ver:/;

         next if /^\+Ins:/ .. /^-Ins:/;
         next if /^\+Del:/ .. /^-Del:/;

         chomp;
         my $gtag = undef;
         $gtag = 1 if /^[\+\-]\S+:/;
         s/^\s+$// if /^=/;

         if (/^=Pkg:\s*(.+?)\s+(.+?)\s+(.+?)\s+(.+?)$/) {
            my $id = join('-', ($1, $2, $3, $4));
            if (not exists $packages{$id}) {
               warn "packages.en.gz references a package that wasn't in packages.gz: $id\n";
               $c = undef;
            } else {
               $c = $packages{$id};
            }
         } elsif (/^=Sum:\s*(.+)$/) {
            $c->{summary} = $1;
         } elsif (/^\+Des:/ .. /^\-Des:/ and not $gtag) {
            $c->{description} .= $_;
         }
      }
      close($z);
      close($sh);
      $progress->update($length) if $progress;
   }

   # post-process on REF
   foreach my $p (grep { exists $packages{$_}->{REF} } keys %packages) {
      my $pid = join('-', map { $packages{$p}->{$_} } qw(name version release arch));
      my $ref = $packages{$p}->{REF};
      my $id = join("-", ($ref->{name}, $ref->{version}, $ref->{release}, $ref->{arch}));
      die "cannot resolve REF to \"$id\" in \"$pid\"" unless exists $packages{$id};
      my $source = $packages{$id};
      while (my ($k, $v) = each(%$source)) {
         $packages{$p}->{$k} = $v unless (exists $packages{$p}->{$k} or not defined $v);
      }
      delete $packages{$p}->{REF};
   }

   # post-process on sourcerpm
   foreach my $p (grep { exists $packages{$_}->{sourcerpm} } keys %packages) {
      my $s = $packages{$p}->{sourcerpm};
      my $rpm = join('-', ($s->{name}, $s->{version}, $s->{release})).".".$s->{arch}.".rpm";
      $packages{$p}->{sourcerpm} = $rpm;
   }

   # implement the proper id field
   foreach my $p (values %packages) {
      $p->{id} = $r->{repoid} . '-' . $p->{sha};
   }

   my @result = values %packages;
   return [\@result, $ri];
}

1;
 
