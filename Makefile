SOLR_VERSION=1.4.1
SOLR_URL=http://www.apache.org/dist/lucene/solr/$(SOLR_VERSION)/apache-solr-$(SOLR_VERSION).tgz
SOLR_SVN=http://svn.apache.org/repos/asf/lucene/dev/trunk/solr/
SOLR_DIR=apache-solr-$(SOLR_VERSION)

TOP=$(shell pwd)

all:	solr.sh

$(SOLR_DIR):
	@case $(SOLR_VERSION) in \
		trunk|latest|head) svn export "$(SOLR_SVN)" "$(SOLR_DIR)" ;; \
		*.*) curl "$(SOLR_URL)" | tar xf - ;; \
		[1-9]*) svn export -r $(SOLR_VERSION) "$(SOLR_SVN)" "$(SOLR_DIR)" ;; \
		*) echo "unsupported SOLR_VERSION $(SOLR_VERSION)" >&2; exit 1 ;; \
	esac
	cp -rv solr/conf/* $(SOLR_DIR)/example/solr/conf/
	patch -p0 -d $(SOLR_DIR) -i $(TOP)/solr/jetty-host.patch

solr.sh:	solr.sh.in $(SOLR_DIR)
	sed 's|@@SOLR_DIR@@|$(SOLR_DIR)|g' <$< >$@
	chmod 0755 $@

start:	solr.sh
	exec ./solr.sh

