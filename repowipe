#!/usr/bin/perl
# vim: set ai et sw=3 ts=3 nu:
#
# Wipes all documents of a repository from the index.
#
# by Pascal Bleser <pascal.bleser@opensuse.org>
#
#     This library is free software; you can redistribute it and/or modify it
#     under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation; either version 2.1 of the License, or (at
#     your option) any later version.
#                 
#     This library is distributed in the hope that it will be useful, but
#     WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.
#      
#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307,
#     USA.

use strict;
use warnings;
use File::Spec;
use File::Basename;
use HTML::Entities ();
use Getopt::Long;
use WebService::Solr;

my $repos = "./repos.d";
my $cache_dir = "./cache.d";
my $verbose = undef;
my $force = undef;

GetOptions(
   'v|verbose' => \$verbose,
);

my $solr_escape_chars = quotemeta( '+-&|!(){}[]^"~*?:\\' );
my @repos = ();

my @rfiles = ();
if (scalar(@ARGV) > 0) {
   push(@rfiles, @ARGV);
} else {
   @rfiles = grep { -f } glob($repos.'/*.conf');
}

foreach my $rfile (@rfiles) {
   open(my $fh, '<', $rfile) or die "failed to open $rfile: $!";
   while (<$fh>) {
      chomp;
      s/#.*$//;
      s/^\s*//;
      s/\s*//;
      next if /^$/;
      if (/^(\S+)\s+(\S+)\s+(\S+)(?:\s+(\S+))?(?:\s+(\S+))?$/) {
         my $r = {
            repoid         => $1,
            distribution   => $2,
            baseurl        => $3,
            configfile     => $rfile,
         };
         $r->{mdtype} = defined $5 ? $5 : 'rpmmd';
         if (defined $6) {
            my %flags = map { $_ => 1 } split(/\s*,\s*/, $6);
            $r->{flags} = \%flags;
         } else {
            $r->{flags} = {};
         }

         push(@repos, $r);
      } else {
         die "invalid repo spec in $rfile at line $.";
      }
   }
   close($fh);
}

my $solr = WebService::Solr->new("http://localhost:8983/solr", {
    autocommit => 0,
});
$solr->ping() or die "failed to ping Solr";

sub solr_escape($) {
   my $v = shift;
   $v =~ s{([$solr_escape_chars])}{\\$1}g;
   return $v;
}

foreach my $r (@repos) {
   print $r->{repoid}, "\n" if $verbose;

   my $cache = File::Spec->catfile($cache_dir, $r->{repoid}.".cache");
   {
      my $dir = dirname($cache);
      mkdir($dir, 0750) unless -d $dir;
   }
   
   my $solr_repoid = solr_escape($r->{repoid});
   $solr->delete_by_query('repoid:'.$solr_repoid) or die "failed to delete repoid:".$solr_repoid;

   unlink($cache) if -f $cache;
}

if (scalar(@repos) > 0) {
   print "committing Solr", "\n" if $verbose;
   $solr->commit();
}

print "optimizing Solr index", "\n" if $verbose;
$solr->optimize();

