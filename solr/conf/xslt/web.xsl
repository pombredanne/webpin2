<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

  <xsl:output media-type="text/html; charset=UTF-8" encoding="UTF-8"/> 
  
  <xsl:variable name="title" select="concat('Solr search results (',response/result/@numFound,' documents)')"/>
  
  <xsl:template match='/'>
    <xsl:variable name="q"><xsl:value-of select="/response/*[@name='responseHeader']/*[@name='params']/*[@name='q']/text()"/></xsl:variable>
    <xsl:variable name="fq"><xsl:value-of select="/response/*[@name='responseHeader']/*[@name='params']/*[@name='fq']/text()"/></xsl:variable>
    <html>
      <head>
        <title><xsl:value-of select="$title"/></title>
        <xsl:call-template name="css"/>
      </head>
      <body>
          <h1><xsl:value-of select="$title"/></h1>
          <div id="search">
              <form action="/solr/select" method="get">
                  <input type="hidden" name="fl" value="*,score"/>
                  <input type="hidden" name="wt" value="xslt"/>
                  <input type="hidden" name="tr" value="web.xsl"/>
                  <input type="hidden" name="qt" value="dismax"/>
                  <input type="hidden" name="hl" value="true"/>
                  <input type="hidden" name="hl.fl" value="*"/>
                  <label for="q">Query:</label>
                  <input id="q" type="text" name="q" value="{$q}"/>
                  <br/>
                  <label for="fq">Filter:</label>
                  <input id="fq" type="text" name="fq" value="{$fq}"/>
                  <br/>
                  <input type="submit" value="Search"/>
              </form>
          </div>
          <div id="perf">Query performed in <xsl:value-of select="/response/*[@name='responseHeader']/*[@name='QTime']/text()"/> milliseconds</div>
          <xsl:apply-templates select="response/result/doc"/>
      </body>
    </html>
  </xsl:template>
  
  <xsl:template match="doc">
    <xsl:variable name="pos" select="position()"/>
    <div class="doc">
        <h2><xsl:value-of select="*[@name='name']/text()"/><xsl:text> </xsl:text><xsl:value-of select="*[@name='version']/text()"/>-<xsl:value-of select="*[@name='release']"/><xsl:text> - </xsl:text><xsl:value-of select="*[@name='summary']/text()"/></h2>
        <p><xsl:value-of select="*[@name='description']"/></p>
        <dl>
            <!--
            <dt>Score</dt>
            <dd><xsl:value-of select="*[@name='score']/text()"/></dd>
            -->
            <dt>Distribution</dt>
            <dd><xsl:value-of select="*[@name='distname']/text()"/><xsl:text> </xsl:text><xsl:value-of select="*[@name='distversion']/text()"/></dd>
            <dt>Arch</dt>
            <dd><xsl:value-of select="*[@name='arch']/text()"/></dd>
            <dt>SHA1</dt>
            <dd><code><xsl:value-of select="*[@name='sha']/text()"/></code></dd>
            <xsl:apply-templates select="arr[@name='mime']"/>
            <xsl:apply-templates select="arr[@name='perlmod']"/>
            <xsl:apply-templates select="arr[@name='file']"/>
            <xsl:apply-templates select="arr[@name='provides']"/>
            <xsl:apply-templates select="arr[@name='requires']"/>
        </dl>
    </div>
  </xsl:template>

  <xsl:template match="arr[@name='mime']">
      <dt>MIME types</dt>
      <dd><ul><xsl:apply-templates select="str"/></ul></dd>
  </xsl:template>

  <xsl:template match="arr[@name='perlmod']">
      <dt>Perl modules</dt>
      <dd><ul><xsl:apply-templates select="str"/></ul></dd>
  </xsl:template>

  <xsl:template match="arr[@name='file']">
      <dt>Files</dt>
      <dd><pre><xsl:apply-templates select="str" mode="files"/></pre></dd>
  </xsl:template>

  <xsl:template match="arr[@name='requires']">
      <dt>Requires</dt>
      <dd><xsl:apply-templates select="str"/></dd>
  </xsl:template>

  <xsl:template match="arr[@name='provides']">
      <dt>Provides</dt>
      <dd><xsl:apply-templates select="str"/></dd>
  </xsl:template>

  <xsl:template match="arr/str">
      <li><xsl:value-of select="text()"/></li>
  </xsl:template>

  <xsl:template match="arr/str" mode="files"><xsl:value-of select="text()"/><xsl:text>
</xsl:text></xsl:template>

  <xsl:template match="*"/>
  
  <xsl:template name="css">
    <style type="text/css">
      body { font-family: "Lucida Grande", sans-serif }
      ul { margin: 0px; margin-left: 1em; padding: 0px; }
      h2 { margin-bottom: 0.2em; }
      .doc { margin-top: 1em; border-top: solid grey 1px; }
      .doc dt { font-weight: bold; float: left; text-decoration: underline; padding-right: 1em; }
      .doc dt:after { content: ":"; }
      .doc dd { clear: right; }
      .doc dd ul { display: inline; }
      .doc dd li { display: inline; }
      .doc dd li:after { content: ","; }
      .doc dd li:last-child:after { content: ""; }
      #perf { color: #666; }
    </style>
  </xsl:template>

</xsl:stylesheet>
