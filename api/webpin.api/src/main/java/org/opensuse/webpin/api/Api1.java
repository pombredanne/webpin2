/*
 * Created on Aug 3, 2010
 */
package org.opensuse.webpin.api;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Status;
import org.restlet.ext.freemarker.TemplateRepresentation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;
import org.restlet.service.MetadataService;
import org.springframework.beans.factory.annotation.Required;

import freemarker.template.Configuration;

public class Api1 extends SolrQueryRestlet {
	
	private QueryMethod queryMethod;
	private Configuration freemarker;
	
	@Override
	public void handle(final Request request, final Response response) {
		final Method method = request.getMethod();
		if (method.equals(Method.GET)) {
			get(request, response);
		} else if (method.equals(Method.HEAD)) {
			head(request, response);
		} else {
			response.setStatus(Status.CLIENT_ERROR_METHOD_NOT_ALLOWED, String.format("the method %s is not supported", method.getName()));
		}
	}
	
	private static final MetadataService METADATA_SERVICE = new MetadataService();
	private static final List<Variant> VARIANTS;
	static {
		VARIANTS = new LinkedList<Variant>();
		VARIANTS.add(new Variant(MediaType.APPLICATION_XML));
		VARIANTS.add(new Variant(MediaType.TEXT_XML));
		VARIANTS.add(new Variant(MediaType.APPLICATION_JSON));
		VARIANTS.add(new Variant(MediaType.TEXT_PLAIN));
		VARIANTS.add(new Variant(MediaType.TEXT_CSV));
		VARIANTS.add(new Variant(MediaType.TEXT_HTML));
	}

	protected final void head(final Request request, final Response response) throws ResourceException {
        final QueryResult r = query(queryMethod, request, response);
        if (r == null) {
        	return;
        }

		if (r.results.getNumFound() < 1) {
			response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
		}

		response.setEntity(new StringRepresentation("", MediaType.TEXT_PLAIN));
	}
	
    //@Get("xml|json|txt|csv|html")
	public final void get(final Request request, final Response response) throws ResourceException {
        final String template;
        final MediaType mediaType;
        {
        	final String format = getStringAttribute(request, "format", null);
        	if (format != null) {
        		if (format.equals("json")) {
        			mediaType = MediaType.APPLICATION_JSON;
        		} else if (format.equals("txt")) {
        			mediaType = MediaType.TEXT_PLAIN;
        		} else if (format.equals("csv")) {
        			mediaType = MediaType.TEXT_CSV;
        		} else if (format.equals("html")) {
        			mediaType = MediaType.TEXT_HTML;
        		} else {
        			mediaType = MediaType.TEXT_XML;
        		}
        	} else {
        		mediaType = request.getClientInfo().getPreferredVariant(VARIANTS, METADATA_SERVICE).getMediaType();
        	}
            if (mediaType.equals(MediaType.APPLICATION_JSON)) {
                template = "api1.json";
            } else if (mediaType.equals(MediaType.TEXT_PLAIN)) {
            	template = "api1.txt";
            } else if (mediaType.equals(MediaType.TEXT_CSV)) {
            	template = "api1.csv";
            } else if (mediaType.equals(MediaType.TEXT_HTML)) {
            	template = "api1.html";
            } else {
                template = "api1.xml";
            }
        }
        
        final QueryResult r = query(queryMethod, request, response);
        if (r == null) {
        	return;
        }

		final Map<String, Object> model = new HashMap<String, Object>(1);
		model.put("packages", r.results);
		model.put("q", r.query);
		
		if (r.results.getNumFound() < 1) {
			response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
		}
		
		response.setEntity(new TemplateRepresentation(template, freemarker, model, mediaType));
	}
    
	@Required public void setQueryMethod(QueryMethod queryMethod) {
		this.queryMethod = queryMethod;
	}
	
	@Required public void setFreemarker(Configuration freemarker) {
		this.freemarker = freemarker;
	}
    
}