/*
 * Created on Aug 3, 2010
 */
package org.opensuse.webpin.api;

import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.opensuse.webpin.api.SolrQueryRestlet.QueryMethod;
import org.restlet.Request;
import org.restlet.Response;

public class ByContentQueryMethod implements QueryMethod {

	@Override public String configure(final Request request, final Response response, final SolrQueryRestlet rsc, final SolrQuery query, final List<String> filterQueries) {
		final String content = rsc.getStringAttribute(request, response, "content");
		if (content.contains("/")) {
			query.set("q.alt", "fqfile:" + content);
		} else {
			query.set("q.alt", "file:" + content);
		}
		return "by file: " + content;
	}
	
}
