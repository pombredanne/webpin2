/*
 * Created on Aug 3, 2010
 */
package org.opensuse.webpin.api;

import java.net.ConnectException;
import java.util.LinkedList;
import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.data.Status;
import org.springframework.beans.factory.annotation.Required;

public abstract class SolrQueryServerResource extends Restlet {

	private static final String[] EMPTY_STRING_ARRAY = {};

	private int defaultRows;
	private String solrQueryType;
	private SolrServer solrServer;

	@Required public void setDefaultRows(int defaultRows) {
		this.defaultRows = defaultRows;
	}

	@Required public void setSolrQueryType(String solrQueryType) {
		this.solrQueryType = solrQueryType;
	}

	@Required public void setSolrServer(SolrServer solrServer) {
		this.solrServer = solrServer;
	}

	public static interface QueryMethod {
		String configure(Request request, Response response, SolrQueryServerResource rsc, SolrQuery query, List<String> filterQueries);
	}

	protected final String getStringAttribute(final Request request, final String name, final String defaultValue) {
        Object o = request.getAttributes().get(name);
        if (o == null) {
            o = request.getResourceRef().getQueryAsForm().getFirstValue(name);
        }
        if (o == null) {
        	return defaultValue;
        } else {
            return o.toString();
        }
	}

	protected final String getStringAttribute(final Request request, final Response response, final String name) {
        Object o = request.getAttributes().get(name);
        if (o == null) {
            o = request.getResourceRef().getQueryAsForm().getFirstValue(name);
        }
        if (o == null) {
            response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST, "missing parameter: " + name);
            throw new IllegalArgumentException("missing parameter: " + name);
        } else {
            return o.toString();
        }
	}

	protected final int getIntAttribute(final Request request, final Response response, final String name, final int defaultValue) {
		final String s;
		{
	        Object o = request.getAttributes().get(name);
	        if (o == null) {
	            o = request.getResourceRef().getQueryAsForm().getFirstValue(name);
	        }
	        if (o == null) {
	        	return defaultValue;
	        } else {
	            s = o.toString();
	        }
		}
		try {
			return Integer.parseInt(s);
		} catch (final NumberFormatException e) {
            response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST, "invalid value for numeric parameter " + name + ": " + s);
            throw new IllegalArgumentException("invalid value for numeric parameter " + name + ": " + s);
		}
	}

	protected final int getIntAttribute(final Request request, final Response response, final String name) {
		final String s = getStringAttribute(request, response, name);
		try {
			return Integer.parseInt(s);
		} catch (final NumberFormatException e) {
            response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST, "invalid value for numeric parameter " + name + ": " + s);
            throw new IllegalArgumentException("invalid value for numeric parameter " + name + ": " + s);
		}
	}

	protected static final class QueryResult {
		public final SolrDocumentList results;
		public final String query;
		public QueryResult(final QueryResponse response, final String query) {
			this.results = response.getResults();
			this.query = query;
		}
	}

	protected final SolrQueryServerResource.QueryResult query(final QueryMethod queryMethod, final Request request, final Response response) {
	    final int start = getIntAttribute(request, response, "start", 0);
	    final int rows = getIntAttribute(request, response, "rows", defaultRows);
	    final String distVersion;
	    final String distName;
	    {
		    final String dist = getStringAttribute(request, "distribution", null);
	    	if (dist != null) {
    			final int index = dist.indexOf("_");
    			if (index > 0) {
    				distName = dist.substring(0, index);
    				{
    					final String v = dist.substring(index + 1);
    					if (v.contains(".")) {
    						distVersion = v;
    					} else {
    						distVersion = v.substring(0, 2) + "." + v.substring(2);
    					}
    				}
    			} else {
    				distName = dist;
    				distVersion = null;
    			}
	    	} else {
	    		distVersion = null;
	    		distName = null;
	    	}
	    }

	    final SolrQuery query = new SolrQuery()
	    .setQuery("")
		.setQueryType(solrQueryType)
		.setFields("*")
		.setIncludeScore(true)
		.setStart(start)
		.setRows(rows)
		;

	    final List<String> filterQueries = new LinkedList<String>();
	    final String queryDescription = queryMethod.configure(request, response, this, query, filterQueries);

	    if (distName != null) {
	    	filterQueries.add("distname:"+distName);
	    }
	    if (distVersion != null) {
	    	filterQueries.add("distversion:"+distVersion);
	    }
    	if (! filterQueries.isEmpty()) {
    		query.setFilterQueries(filterQueries.toArray(EMPTY_STRING_ARRAY));
    	}

		final QueryResponse resp;
		try {
			resp = solrServer.query(query, METHOD.GET);
		} catch (final SolrServerException e) {
			if (e.getCause() instanceof ConnectException) {
				response.setStatus(Status.SERVER_ERROR_SERVICE_UNAVAILABLE, "failed to contact Solr");
			} else {
				response.setStatus(Status.SERVER_ERROR_INTERNAL, "failed to query Solr: " + e.getMessage());
			}
            return null; // make compiler happy
		}
		return new QueryResult(resp, queryDescription);
	}

}