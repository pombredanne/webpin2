/*
 ******************************************************************************
 *      Atos Worldline GmbH                Common Development
 *      Pascalstrasse 19                   Phone +49 (0) 2408/148-0
 *      D-52076 Aachen
 *
 *      Atos Worldline is an Atos Origin company
 *      All rights reserved
 *
 ******************************************************************************
 */
package org.opensuse.webpin.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Reference;
import org.restlet.data.Status;
import org.restlet.ext.freemarker.TemplateRepresentation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import freemarker.template.Configuration;

public class SearchUI extends SolrQueryRestlet {

    private QueryMethod queryMethod;
    private Configuration freemarker;
    private int maxPages = 10;

    private static final Map<String, String> DISTS;
    static {
    	DISTS = Maps.newLinkedHashMap();
    	DISTS.put("openSUSE_11.3", "openSUSE 11.3");
    	DISTS.put("openSUSE_11.2", "openSUSE 11.2");
    	DISTS.put("openSUSE_11.1", "openSUSE 11.1");
    }

    private static final Map<String, String> ARCHS;
    static {
    	ARCHS = Maps.newLinkedHashMap();
    	ARCHS.put("i586", "i586 (32 bit)");
    	ARCHS.put("i686", "i686 (32 bit)");
    	ARCHS.put("x86_64", "x86_64 (64 bit)");
    	ARCHS.put("noarch", "noarch (any)");
    	ARCHS.put("src", "source packages");
    }

    private static final Map<String, String> TYPES;
    static {
    	TYPES = Maps.newLinkedHashMap();
    	TYPES.put("lib", "libraries");
    	TYPES.put("devel", "development");
    	TYPES.put("doc", "documentation");
    	TYPES.put("lang", "translations");
        TYPES.put("debug", "debug");
    }

    private static final String[] FACET_FIELDS = { "distribution", "arch", "tag", "repoid" };
    private static final Set<String> HIDDEN_WHEN_ZERO_FACET_FIELDS = new HashSet<String>(Arrays.asList("repoid"));
    private static final Map<String, String> FACET_LABELS = Maps.newHashMapWithExpectedSize(FACET_FIELDS.length);
    private static final Map<String, Function<String, String>> FACET_VALUE_TRANSFORMERS = Maps.newHashMapWithExpectedSize(FACET_FIELDS.length);
    private static final Map<String, Map<String, String>> FACET_VALUE_LABELS = Maps.newHashMapWithExpectedSize(FACET_FIELDS.length);

    static {
    	FACET_LABELS.put("arch", "architecture");
    	FACET_LABELS.put("tag", "type");
    	FACET_LABELS.put("repoid", "repository");

    	FACET_VALUE_LABELS.put("tag", TYPES);
    	FACET_VALUE_LABELS.put("arch", ARCHS);

    	FACET_VALUE_TRANSFORMERS.put("distribution", new Function<String, String>() {
			@Override public String apply(final String from) {
				return from.replace("_", " ");
			}
		});
    }

    private static final String[] PREFIXED_FACET_FIELDS = new String[FACET_FIELDS.length];
    static {
    	for (int i = 0; i < FACET_FIELDS.length; i++) {
    		PREFIXED_FACET_FIELDS[i] = "{!ex=ff}" + FACET_FIELDS[i];
    	}
    }

    private static final Pattern OBS_REPO_PATTERN = Pattern.compile("^(?:http|https|ftp)://download\\.opensuse\\.org/repositories/(.+)/(?:.+)/?$");
    private static final Pattern OSS_REPO_PATTERN = Pattern.compile("^(?:http|https|ftp)://download\\.opensuse\\.org/distribution/(?:[^/]+)/repo/oss/?$");
    private static final Pattern NON_OSS_REPO_PATTERN = Pattern.compile("^(?:http|https|ftp)://download\\.opensuse\\.org/distribution/(?:[^/]+)/repo/non-oss/?$");
    private static final Map<Pattern, String> REPO_URL_PATTERNS = new HashMap<Pattern, String>() {{
        put(OBS_REPO_PATTERN, "%s");
        put(OSS_REPO_PATTERN, "oss");
        put(NON_OSS_REPO_PATTERN, "non-oss");
    }};

    private static final String[] HIGHLIGHT_FIELDS = { "name", "summary", "file", "fqfile", "description", };
    private static final String[] POST_PROC_MULTIVALUED = { "tag" };

    private final QueryMethod highlightingQueryMethod = new QueryMethod() {
        @Override
        public String configure(Request request, Response response, SolrQueryRestlet rsc, SolrQuery query, List<String> filterQueries) {
            final String name = queryMethod.configure(request, response, rsc, query, filterQueries);

            query
            .setHighlight(true)
            .setHighlightFragsize(300)
            .setHighlightSnippets(3)
            .setHighlightSimplePre("<em>")
            .setHighlightSimplePost("</em>")
            ;
            for (final String highlightField : HIGHLIGHT_FIELDS) {
                query.addHighlightField(highlightField);
            }

            query.setFacet(true);
            query.addFacetField(PREFIXED_FACET_FIELDS);
            query.setFacetLimit(10);
            query.setFacetMinCount(0);
            query.setFacetMissing(false);

            return name;
        }
    };

    private final QueryMethod archQueryMethod = new QueryMethod() {
        @Override
        public String configure(Request request, Response response, SolrQueryRestlet rsc, SolrQuery query, List<String> filterQueries) {
            final String name = highlightingQueryMethod.configure(request, response, rsc, query, filterQueries);
            for (final String tagParam : TYPES.keySet()) {
            	if (getIntAttribute(request, response, tagParam, 0) == 0) {
	            	filterQueries.add("-tag:" + tagParam);
	            }
            }
            return name;
        }
    };


    @Override
    public void handle(final Request request, final Response response) {
        final Method method = request.getMethod();
        if (method.equals(Method.GET)) {
            get(request, response);
        } else {
            response.setStatus(Status.CLIENT_ERROR_METHOD_NOT_ALLOWED, String.format("the method %s is not supported", method.getName()));
        }
    }

    private static final List<Variant> VARIANTS;
    static {
        VARIANTS = new LinkedList<Variant>();
        VARIANTS.add(new Variant(MediaType.TEXT_HTML));
    }

    protected final void head(final Request request, final Response response) throws ResourceException {
        final QueryResult r = query(queryMethod, request, response);
        if (r == null) {
            return;
        }

        if (r.results.getNumFound() < 1) {
            response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
        }

        response.setEntity(new StringRepresentation("", MediaType.TEXT_PLAIN));
    }

    private static final Comparator<FacetField.Count> FACET_COMPARATOR = new Comparator<FacetField.Count>() {
        @Override public int compare(final FacetField.Count o1, final FacetField.Count o2) {
            return (int) (o2.getCount() - o1.getCount());
        }
    };

    public static final class FacetValueModel implements Serializable {
    	public final String name;
    	public final String label;
    	public final long count;
		public FacetValueModel(final String name, final String label, final long count) {
			this.name = name;
			this.label = label != null ? label : name;
			this.count = count;
		}
		public String getName() {
			return name;
		}
		public String getLabel() {
			return label;
		}
		public long getCount() {
			return count;
		}
    }

    public static final class FacetModel implements Serializable {
    	public final String name;
    	public final String label;
    	public final List<FacetValueModel> values;
		public FacetModel(final String name, final String label, final List<FacetValueModel> values) {
			this.name = name;
			this.label = label != null ? label : name;
			this.values = values;
		}
		public String getName() {
			return name;
		}
		public String getLabel() {
			return label;
		}
		public List<FacetValueModel> getValues() {
			return values;
		}
    }

    public final void get(final Request request, final Response response) throws ResourceException {
        final QueryResult r = query(archQueryMethod, request, response);
        if (r == null) {
            return;
        }

        final Map<String, Object> model = new HashMap<String, Object>();

        final List<Map<String, Object>> packages;
        {
            final List<Map<String, Object>> highlightedPackages;
            {
                highlightedPackages = new ArrayList<Map<String,Object>>(r.results.size());
                for (final Map<String, Object> pkg : r.results) {
                    final Object id = pkg.get("id");
                    final Map<String, Object> modelPkg;
                    {
                        final Map<String, List<String>> pkgHighlightMap = r.highlighting.get(id);
                        if (pkgHighlightMap != null) {
                            modelPkg = new HashMap<String, Object>(pkg.size());
                            for (final Entry<String, Object> e : pkg.entrySet()) {
                                final List<String> fieldHighlight = pkgHighlightMap.get(e.getKey());
                                if (fieldHighlight != null) {
                                    modelPkg.put(e.getKey(), Joiner.on("&hellip;").join(fieldHighlight));
                                } else {
                                    modelPkg.put(e.getKey(), e.getValue());
                                }
                            }
                        } else {
                            modelPkg = pkg;
                        }
                    }
                    highlightedPackages.add(modelPkg);
                }
            }
            final List<Map<String, Object>> postProcessedPackages = Lists.transform(highlightedPackages, new Function<Map<String, Object>, Map<String, Object>>() {
                @SuppressWarnings("unchecked")
				@Override public Map<String, Object> apply(final Map<String, Object> pkg) {
                	if (pkg.containsKey("repourl")) {
                        final String repotag;
                    	if (pkg.get("repoid").toString().startsWith("packman-")) {
	                        repotag = "Packman";
	                    } else {
	                        String t = null;
	                        for (final Entry<Pattern, String> e : REPO_URL_PATTERNS.entrySet()) {
	                            final Matcher m = e.getKey().matcher(pkg.get("repourl").toString());
	                            if (m.matches()) {
	                                t = String.format(e.getValue(), m.group());
	                                break;
	                            }
	                        }
	                        repotag = t;
	                    }
	                    pkg.put("repotag", repotag);
                    }
                    {
                    	for (final String field : POST_PROC_MULTIVALUED) {
                    		final Object o = pkg.get(field);
                    		if (o != null) {
                    			if (o instanceof Collection<?>) {
                    				final List<Object> list = Lists.newArrayList((Collection<Object>) o);
                    				pkg.put(field, list);
                    			} else {
                    				pkg.put(field, Lists.newArrayList(o));
                    			}
                    		}
                    	}
                    }
                    return pkg;
                }
            });
            packages = postProcessedPackages;
        }

        model.put("packages", packages);
        model.put("q", r.query);

        // facets
        {
        	final List<FacetModel> facetModels = Lists.newLinkedList();
            for (final FacetField facet : r.facets) {
            	final List<FacetValueModel> facetValueModels = Lists.newLinkedList();
            	{
	            	final List<Count> counts = facet.getValues();
	                if (counts != null) {
		                Collections.sort(counts, FACET_COMPARATOR);
                		final Function<String, String> labelTransformer = FACET_VALUE_TRANSFORMERS.get(facet.getName());
                		final Map<String, String> labelMap = FACET_VALUE_LABELS.get(facet.getName());
		                for (final Count c : counts) {
		                	if (c.getCount() > 0 || (! HIDDEN_WHEN_ZERO_FACET_FIELDS.contains(facet.getName()))) {
		                		final String label;
		                		{
		                			String l = null;
		                			if (labelMap != null) {
		                				l = labelMap.get(c.getName());
		                			}
		                			if (l == null && labelTransformer != null) {
		                				l = labelTransformer.apply(c.getName());
		                			}
		                			label = l != null ? l : c.getName();
		                		}
		                		facetValueModels.add(new FacetValueModel(c.getName(), label, c.getCount()));
		                	}
		                }
	                }
            	}
            	if (! facetValueModels.isEmpty()) {
            		facetModels.add(new FacetModel(facet.getName(), FACET_LABELS.get(facet.getName()), facetValueModels));
            	}
            }
            if (! facetModels.isEmpty()) {
            	model.put("facets", facetModels);
            }
        }

    	final List<String> tags = new LinkedList<String>();
        {
	        for (final String tagParam : TYPES.keySet()) {
	        	final int value = getIntAttribute(request, response, tagParam, 0);
	        	model.put(tagParam, value);
	        	if (value != 0) {
	        		tags.add(tagParam);
	        	}
	        }
	        final Map<String, List<String>> params = Maps.newHashMap();
	        params.put("tag", tags);
	        params.put("distribution", r.dists);
	        params.put("arch", r.archs);
	        params.put("repoid", r.repoids);
	        model.put("params", params);
        }

        if (r.results.getNumFound() < 1) {
            response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
        }

        model.put("qtime", r.qtime);

        // paging
        final int start = (int) r.start;
        final int total = (int) r.results.getNumFound();
        final int rows = (int) (r.rows >= 0 ? r.rows : this.defaultRows);
        final int pages = total > 0 ? (total / rows) + 1 : 0;
        final int currentPage = (start / rows) + 1;
        model.put("hits", total);
        model.put("start", start);
        model.put("rows", rows);
        model.put("pages", pages);
        model.put("currentPage", currentPage);

        // compute pages here instead of in the view layer
        // reconstruct:
        final String uriTemplate;
        {
        	final Reference ref = new Reference(request.getResourceRef().getBaseRef());
        	ref.addQueryParameter("q", r.query);
        	for (final String dist : r.dists) {
        		ref.addQueryParameter("distribution", dist.replace("_", " "));
        	}
        	for (final String arch : r.archs) {
        		ref.addQueryParameter("arch", arch);
        	}
        	for (final String tag : tags) {
        		ref.addQueryParameter(tag, "1");
        	}
    		ref.addQueryParameter("start", "");
    		uriTemplate = ref.toString(true, false);
        }
        final int pagesToShow = pages <= maxPages ? pages : maxPages;
        if (pagesToShow > 1) {
            final List<Map<String, Object>> pagelist = Lists.newArrayListWithCapacity((int) pages);
	        for (int i = 1; i <= pagesToShow; i++) {
	        	final Map<String, Object> attr = Maps.newHashMapWithExpectedSize(3);
	        	final int pageStart = (i - 1) * rows;
	        	attr.put("page", i);
	        	attr.put("url", uriTemplate + pageStart);
	        	attr.put("current", i == currentPage);
	        	pagelist.add(attr);
	        }
	        if (currentPage > 1) {
	        	model.put("previous", pagelist.get((currentPage - 1) - 1));
	        }
	        if (currentPage < pages) {
	        	model.put("next", pagelist.get((currentPage + 1) - 1));
	        }
	        if (pagesToShow < pages) {
	        	model.put("more", true);
	        }
	        model.put("pagelist", pagelist);
        }

        if (r.solrQuery != null) {
            model.put("solrquery", Reference.decode(r.solrQuery));
        }

        response.setEntity(new TemplateRepresentation("ui.html", freemarker, model, MediaType.TEXT_HTML));
    }

    @Required public void setFreemarker(Configuration freemarker) {
        this.freemarker = freemarker;
    }

    @Required public void setQueryMethod(QueryMethod queryMethod) {
        this.queryMethod = queryMethod;
    }

}
