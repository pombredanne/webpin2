/*
 * Created on Aug 3, 2010
 */
package org.opensuse.webpin.api;

import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.opensuse.webpin.api.SolrQueryRestlet.QueryMethod;
import org.restlet.Request;
import org.restlet.Response;

public class FullTextQueryMethod implements QueryMethod {

    private String searchTermAttributeName = "searchTerm";

	@Override public String configure(final Request request, final Response response, final SolrQueryRestlet rsc, final SolrQuery query, final List<String> filterQueries) {
		final String s = rsc.getStringAttribute(request, response, searchTermAttributeName);
		//query.setQuery(String.format("{!lucene qf=name v=%s} {!lucene qf=name_rev v=*%s} {!dismax qf=text v=%s}", s, s, s));
		if (s.startsWith(":")) {
			query.setQuery(s.substring(1));
		} else if (s.contains(":")) {
			query.setQuery(s);
		} else {
			query.setQuery(String.format("name:%s~0.8 OR text:%s", s, s));
		}
		query.setParam("pf", "name^100.0 provides^30.0");
		//System.out.println(query.toString());
		//text:hi  AND  _query_:"{!dismax qf=title pf=title}how now brown cow"
		return s;
	}

	public void setSearchTermAttributeName(String searchTermAttributeName) {
        this.searchTermAttributeName = searchTermAttributeName;
    }

}
