/*
 * Created on Aug 1, 2010
 */
package org.opensuse.webpin.api;

import java.util.concurrent.Semaphore;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.restlet.Component;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public final class Server implements Daemon {

	public static void main(String[] args) throws Exception {
		final Server server = new Server();
		server.init(null);
		server.start();
		try {
			new Semaphore(0).acquire();
		} finally {
			server.stop();
			server.destroy();
		}
	}

	private ConfigurableApplicationContext applicationContext = null;
	private DaemonContext daemon = null;
	
	@Override
	public void init(final DaemonContext d) throws Exception {
	    LoggerFactory.getLogger("query");
	    
		this.daemon = d;
		try {
			applicationContext = new ClassPathXmlApplicationContext(new String[] { "applicationContext.xml" });
		} catch (final BeansException be) {
			final String message = String.format("failed to initialize the %s: %s", ApplicationContext.class.getSimpleName(), be.getMessage());
			if (d != null) {
				d.getController().fail(message, be);
				return;
			} else {
				throw be;
			}
		}
	}

	@Override
	public void start() throws Exception {
		if (applicationContext == null) {
			final String message = String.format("failed to start as the %s is not initialized", ApplicationContext.class.getSimpleName());
			if (daemon != null) {
				daemon.getController().fail(message);
				return;
			} else {
				throw new IllegalStateException(message);
			}
		}
		applicationContext.getBean("top", Component.class).start();		
	}

	@Override
	public void stop() throws Exception {
		if (applicationContext == null) {
			final String message = String.format("failed to stop as the %s is not initialized", ApplicationContext.class.getSimpleName());
			if (daemon != null) {
				daemon.getController().fail(message);
				return;
			} else {
				throw new IllegalStateException(message);
			}
		}
		applicationContext.getBean("top", Component.class).stop();		
	}

	@Override
	public void destroy() {
		if (applicationContext == null) {
			final String message = String.format("failed to destroy as the %s is not initialized", ApplicationContext.class.getSimpleName());
			if (daemon != null) {
				daemon.getController().fail(message);
				return;
			} else {
				throw new IllegalStateException(message);
			}
		}
		applicationContext.close();
		applicationContext = null;
	}
	
}
