/*
 * Created on Aug 3, 2010
 */
package org.opensuse.webpin.api;

import java.io.File;

import org.restlet.data.LocalReference;
import org.restlet.resource.Directory;
import org.restlet.routing.Router;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Required;

public class DefaultDirectoryAttacher implements InitializingBean {
	
	private String directory;
	private Router router;

	@Override public void afterPropertiesSet() throws Exception {
		final File cwd = new File(".").getCanonicalFile();
        final Directory result = new Directory(router.getContext(), LocalReference.createFileReference(new File(cwd, directory)));
        result.setDeeplyAccessible(false);
        result.setListingAllowed(true);
        router.attachDefault(result);
	}

	@Required public void setDirectory(String directory) {
		this.directory = directory;
	}

	@Required public void setRouter(Router router) {
		this.router = router;
	}
	
}
