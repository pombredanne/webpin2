/*
 * Created on Aug 3, 2010
 */
package org.opensuse.webpin.api;

import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.opensuse.webpin.api.SolrQueryRestlet.QueryMethod;
import org.restlet.Request;
import org.restlet.Response;

public class ByNameQueryMethod implements QueryMethod {

	@Override public String configure(final Request request, final Response response, final SolrQueryRestlet rsc, final SolrQuery query, final List<String> filterQueries) {
		final String name = rsc.getStringAttribute(request, response, "name");
		query.set("q.alt", "name:" + name);
		return "by name: " + name;
	}
}
