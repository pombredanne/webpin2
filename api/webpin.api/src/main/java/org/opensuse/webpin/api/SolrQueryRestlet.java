/*
 * Created on Aug 3, 2010
 */
package org.opensuse.webpin.api;

import java.net.ConnectException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.util.NamedList;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.data.Form;
import org.restlet.data.Reference;
import org.restlet.data.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

public abstract class SolrQueryRestlet extends Restlet {

    private static final transient Logger QUERY_LOGGER = LoggerFactory.getLogger("query");

	private static final String[] EMPTY_STRING_ARRAY = {};

	protected int defaultRows;
	private SolrServer solrServer;
	private String solrServerBaseURL = "";

	public void setSolrServerBaseURL(String solrServerBaseURL) {
        this.solrServerBaseURL = solrServerBaseURL;
    }

	@Required public void setDefaultRows(int defaultRows) {
		this.defaultRows = defaultRows;
	}

	@Required public void setSolrServer(SolrServer solrServer) {
		this.solrServer = solrServer;
	}

	public static interface QueryMethod {
		String configure(Request request, Response response, SolrQueryRestlet rsc, SolrQuery query, List<String> filterQueries);
	}

	protected final String getStringAttribute(final Request request, final String name, final String defaultValue) {
        Object o = request.getAttributes().get(name);
        if (o == null) {
            o = request.getResourceRef().getQueryAsForm().getFirstValue(name);
        }
        if (o == null) {
        	return defaultValue;
        } else {
            return o.toString();
        }
	}

	protected final String getStringAttribute(final Request request, final Response response, final String name) {
        Object o = request.getAttributes().get(name);
        if (o == null) {
            o = request.getResourceRef().getQueryAsForm().getFirstValue(name);
        }
        if (o == null) {
            response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST, "missing parameter: " + name);
            throw new IllegalArgumentException("missing parameter: " + name);
        } else {
            return o.toString();
        }
	}

	protected final int getIntAttribute(final Request request, final Response response, final String name, final int defaultValue) {
		final String s;
		{
	        Object o = request.getAttributes().get(name);
	        if (o == null) {
	            o = request.getResourceRef().getQueryAsForm().getFirstValue(name);
	        }
	        if (o == null) {
	        	return defaultValue;
	        } else {
	            s = o.toString();
	        }
		}
		try {
			return Integer.parseInt(s);
		} catch (final NumberFormatException e) {
            response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST, "invalid value for numeric parameter " + name + ": " + s);
            throw new IllegalArgumentException("invalid value for numeric parameter " + name + ": " + s);
		}
	}

	protected final int getIntAttribute(final Request request, final Response response, final String name) {
		final String s = getStringAttribute(request, response, name);
		try {
			return Integer.parseInt(s);
		} catch (final NumberFormatException e) {
            response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST, "invalid value for numeric parameter " + name + ": " + s);
            throw new IllegalArgumentException("invalid value for numeric parameter " + name + ": " + s);
		}
	}

	protected static final class QueryResult {
		public final SolrDocumentList results;
		public final String query;
        public final Map<String, Map<String, List<String>>> highlighting;
        public final List<String> dists;
        public final List<String> archs;
        public final List<String> repoids;
        public final List<FacetField> facets;
        public final String solrQuery;
		public final long qtime;
		public final long start;
		public final long rows;
		public QueryResult(final QueryResponse response, final String queryURL, final String query, final List<String> dists, final List<String> archs, final List<String> repoids) {
			this.solrQuery = queryURL;
			this.results = response.getResults();
			this.qtime = response.getElapsedTime();
			this.query = query;
			this.highlighting = response.getHighlighting();
			this.dists = dists;
			this.archs = archs;
			this.repoids = repoids;
			this.start = response.getResults().getStart();
			{
				long r = -1L;
				{
					final NamedList<?> rh = response.getResponseHeader();
					if (rh != null) {
						final Object o = rh.get("params");
						if (o != null && o instanceof NamedList<?>) {
							final Object rowsObj = ((NamedList<?>) o).get("rows");
							if (rowsObj != null) {
								try {
									r = Integer.parseInt(rowsObj.toString());
								} catch (final NumberFormatException e) {
									// suppress
								}
							}
						}

					}
				}
				this.rows = r;
			}
			this.facets = response.getFacetFields();
		}
	}

	protected final SolrQueryRestlet.QueryResult query(final QueryMethod queryMethod, final Request request, final Response response) {
	    final int start = getIntAttribute(request, response, "start", 0);
	    final int rows = getIntAttribute(request, response, "rows", defaultRows);
	    final List<String> dists;
	    final List<String> archs;
	    final List<String> repoids;
    	{
	    	final Form form = request.getResourceRef().getQueryAsForm();
	    	dists = Arrays.asList(form.getValuesArray("distribution"));
	    	archs = Arrays.asList(form.getValuesArray("arch"));
	    	repoids = Arrays.asList(form.getValuesArray("repoid"));
    	}

	    final SolrQuery query = new SolrQuery()
	    .setQuery("")
		.setQueryType("standard")
		.setFields("name^1000,*,score")
		.setStart(start)
		.setRows(rows)
		;

	    final List<String> filterQueries = new LinkedList<String>();
	    final String queryDescription = queryMethod.configure(request, response, this, query, filterQueries);

    	if (! (dists.isEmpty())) {
	    	filterQueries.add("+(" + Joiner.on(" OR ").join(Lists.transform(dists, new Function<String, String>() {
				@Override public String apply(String dist) {
					return "distribution:" + dist;
				}
	    	})) + ")");
    	}

    	if (! (archs.isEmpty() || archs.size() == 3)) {
	    	filterQueries.add("+(" + Joiner.on(" OR ").join(Lists.transform(archs, new Function<String, String>() {
				@Override public String apply(String arch) {
					return "arch:" + arch;
				}
	    	})) + ")");
    	}

    	if (! repoids.isEmpty()) {
	    	filterQueries.add("+(" + Joiner.on(" OR ").join(Lists.transform(repoids, new Function<String, String>() {
				@Override public String apply(String repoid) {
					return "repoid:" + repoid;
				}
	    	})) + ")");
    	}

    	if (! filterQueries.isEmpty()) {
    		query.setFilterQueries(Lists.transform(filterQueries, new Function<String, String>() {
                @Override public String apply(final String from) {
                    return "{!tag=ff}" + from;
                }
            }).toArray(EMPTY_STRING_ARRAY));
    	}

    	if (QUERY_LOGGER.isDebugEnabled()) {
    	    QUERY_LOGGER.debug(String.format("%s/%s", solrServerBaseURL, Reference.decode(query.toString())));
    	}
        System.out.println(String.format("%s/%s", solrServerBaseURL, Reference.decode(query.toString())));

		final QueryResponse resp;
		try {
			resp = solrServer.query(query, METHOD.GET);
		} catch (final SolrServerException e) {
			if (e.getCause() instanceof ConnectException) {
				response.setStatus(Status.SERVER_ERROR_SERVICE_UNAVAILABLE, "failed to contact Solr");
			} else {
				response.setStatus(Status.SERVER_ERROR_INTERNAL, "failed to query Solr: " + e.getMessage());
			}
            return null; // make compiler happy
		}
		return new QueryResult(resp, query.toString(), queryDescription, dists, archs, repoids);
	}

}